const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

// this setup  express app
const app = express();

app.use(express.static(__dirname + '/public'));
// this log request to the console 
app.use(logger('dev'));


// this parse incoming request data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// this setup a default catch-all route that sends back a welcome message in JSON format.
require('./server/routes')(app);
module.exports = app