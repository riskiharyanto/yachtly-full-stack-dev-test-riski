angular.module('Home', ['Alertify'])
  .controller('HomeController', function ($scope, $window) {

    $scope.showData = function(){
      var path = '/users';
        $window.location.href = path;  
    };

    $scope.addData = function(){
        var path = '/user/add';
          $window.location.href = path;  
    };
  });