angular.module('InputData', ['Alertify'])
  .controller('InputDataController', function ($scope, $http,  Alertify) {
    $scope.formData = {};
    $scope.inputData = function(){
      $http({
        method: 'POST',
        url: '/api/user',
        data: {
          name: $scope.formData.name,
          email: $scope.formData.email,
          phoneNumber: $scope.formData.phoneNumber,
          address: $scope.formData.address
        },
        headers: {
            'Content-type': 'application/json;charset=utf-8'
        }
    })
    .then(function(response) {
      console.log(response.data);
      $scope.alert('Input Data Success');
      $scope.formData = {};
    }, function(response) {
      console.log(response);
      $scope.alert(response.data.error);
    });
    }

    $scope.alert = function (msg) {
      Alertify.alert(msg);
    };
  });