angular.module('ShowData', ['Alertify'])
  .controller('ShowDataController', function ($scope, $http, $window, Alertify) {
    $scope.formData = {};

    $scope.refreshData = function() {
      $http({
        method: 'GET',
        url: '/api/user'
      })
      .then(function(response) {
        $scope.users = response.data;
        console.log(response.data);
      }, function(error) {
        if(error.data && error.data.error){
          $scope.alert(error.data.error);
          console.log(error.data.error);
        } else {
          $scope.alert( error.status+" "+error.statusText);
          console.log('Error: ' +  error.status+" "+error.statusText);
        }
      });
    };

    $scope.updateData = function(userID){
      var path = '/user/edit/' + userID;
      $window.location.href = path;
    };

    $scope.deleteData = function(userID) {
      $http({
          method: 'DELETE',
          url: '/api/user',
          data: {
            userID: userID
          },
          headers: {
              'Content-type': 'application/json;charset=utf-8'
          }
      })
      .then(function(response) {
          console.log(response.data);
          $scope.alert("Delete Data Success");
          $scope.refreshData();
      }, function(error) {
        console.log('Error: ' + error);
      });
    };

    $scope.refreshData();

    $scope.alert = function (msg) {
      Alertify.alert(msg);
    };

    $scope.ask = function (userID) {
      Alertify.confirm("Are you sure to delete this data ?")
        .then(function () {
          $scope.deleteData(userID);
        }, function () {
        });
    };
  });