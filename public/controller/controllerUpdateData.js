angular.module('UpdateData', ['Alertify'])
    .controller('UpdateDataController', function ($scope, $http, $location, Alertify) {
    var param;
    var params;
    var absUrl = $location.absUrl();
    params = absUrl.split("/");
    param = params[5];

    $scope.formData = {};
    
    $scope.readOneData = function(param){
        $http({
            method: 'GET',
            url: '/api/user/'+ param
          })
          .then(function(response) {
            $scope.formData = response.data;
            console.log(response.data);
          }, function(error) {
            if(error.data && error.data.error){
              $scope.alert(error.data.error);
              console.log(error.data.error);
            } else {
              $scope.alert( error.status+" "+error.statusText);
              console.log('Error: ' +  error.status+" "+error.statusText);
            }
          });
    }
    
    $scope.updateData = function(){
        $http({
            method: 'PUT',
            url: '/api/user',
            data: {
                userID: $scope.formData.userID,
                name: $scope.formData.name,
                email: $scope.formData.email,
                phoneNumber: $scope.formData.phoneNumber,
                address: $scope.formData.address
            },
            headers: {
                'Content-type': 'application/json;charset=utf-8'
            }
    })
    .then(function(response) {
        console.log(response.data);
        $scope.alert('Updated Data Success');
        $scope.formData = {};
    }, function(error) {
        console.log(error);
        if(error.data && error.data.error){
        $scope.alert(error.data.error);
        } else {
        $scope.alert(error.status+" "+error.statusText);
        }
    });
    }

    $scope.readOneData(param);
    
    $scope.alert = function (msg) {
        Alertify.alert(msg);
    };
    })