const user = require('./user');
const public = require('./public');

module.exports = {
  user,
  public
}