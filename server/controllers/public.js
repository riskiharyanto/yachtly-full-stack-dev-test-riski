module.exports = {
    home(req, res){
        res.sendfile('./public/view/home.html');
    },
    showData(req, res){
        res.sendfile('./public/view/showData.html');
    },
    inputData(req, res){
        res.sendfile('./public/view/inputData.html');
    },
    updateData(req, res){
        res.sendfile('./public/view/updateData.html');
    }
};