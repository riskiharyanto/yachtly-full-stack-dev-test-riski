const User = require('../models').User;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const aliaseOperator = {
  $not : [Op.not]
}

module.exports = {
  create(req, res) {
     User.findAll({
        where:{email:req.body.email}
      })
      .then(user => {
        if (user.length >= 1){
          res.status(403).json({error:"Email has been used"});
        } else if (req.body.email == "" || req.body.email == null){
          res.status(403).json({error:"Email is required and format email must correct"});
        } else if (req.body.name == "" || req.body.name == null){
          res.status(403).json({error:"Name is required"});
        } else {
          User.create({
            userID: null,
            name: req.body.name,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            address: req.body.address
          })
          .then(savingResult => {
            User.update({
              userID: savingResult.id
            },{
              where:{id:savingResult.id}
            })
            .then(finalResult => {
              res.status(200).send(finalResult);
            })
          })
          .catch(error => res.status(400).send(error));
        }
      })
      .catch(error => res.status(400).send(error));
  },

  read(req, res){
    return User
      .findAll()
      .then(user => res.status(200).send(user))
      .catch(error =>  res.status(400).send(error));
  },

  readOne(req, res){
    return User
      .findAll({
        where:{userID:req.params.userID}
      })
      .then(user => res.status(200).json(user[0]))
      .catch(user => res.status(400).send(error));
  },

  update(req, res) {
    let datanew;
    User.findAll({
      where:{ email:req.body.email,
              userID: { 
                $not: req.body.userID
              }
            }
    })
    .then(user => {
      if (user.length >= 1){
        res.status(403).json({error:"Email has been used"});
      } else if (req.body.email == "" || req.body.email == null){
        res.status(403).json({error:"Email is required and format email must correct"});
      } else if (req.body.name == "" || req.body.name == null){
        res.status(403).json({error:"Name is required"});
      } else {
        User.update({
            userID: req.body.userID,
            name: req.body.name,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            address: req.body.address
        },{
          where:{userID:req.body.userID} 
        })
        .then(user => res.status(200).send(user))
        .catch(error => res.status(400).send(error));
        }
    })
    .catch(error => res.status(400).send(error));
  },

  delete(req, res){
    console.log(req.body);
    return User
      .destroy({
        where:{userID:req.body.userID}
      })
      .then(user => res.status(200).json({message:'Success'}))
      .catch(error => res.status(400).json({message:'Error  '}));
  }

};