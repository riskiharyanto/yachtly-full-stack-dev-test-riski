const UsersController = require('../controllers').user;
const PublicController = require('../controllers').public;

module.exports = (app) => {
  
  app.get('/api/user', UsersController.read);

  app.get('/api/user/:userID', UsersController.readOne);

  app.post('/api/user', UsersController.create);

  app.put('/api/user', UsersController.update);

  app.delete('/api/user', UsersController.delete);

  app.get('/user/add', PublicController.inputData);

  app.get('/user/edit/:userID', PublicController.updateData);
  
  app.get('/users', PublicController.showData);

  app.get('*', PublicController.home);

};